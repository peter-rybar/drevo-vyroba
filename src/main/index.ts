import * as fs from "fs";
import { Hsmls } from "prest-lib/dist/hsml";
import { hsmls2htmls, } from "prest-lib/dist/hsml-html";
import { page } from "./components/page";
import { index } from "./components/index";
import { drevodomy } from "./components/drevodomy";
import { krovy } from "./components/krovy";
import { altanky } from "./components/altanky";
import { doplnky } from "./components/doplnky";
import { kontakt } from "./components/kontakt";

function HTML(file: string, jsonmls: Hsmls, pretty = false): void {
    const path = __dirname + "/../../dist/" + file;
    console.log("generate:", path);
    const html = hsmls2htmls(jsonmls, pretty).join("");
    fs.writeFileSync(path, html);
}


const pretty = true;
const lang = "sk";

const titleSite = " | Drevo výroba";

let file = "index.html";
let title = "Drevodomy, zruby, drevenice, krovy, altánky, doplnky" + titleSite;
HTML(file, page(file, title, index(), lang), pretty);

file = "drevodomy.html";
title = "Drevodomy, zruby, drevenice" + titleSite;
HTML(file, page(file, title, drevodomy(), lang), pretty);

file = "krovy.html";
title = "Krovy, sedlové, valbové, pultové krovy, strechy" + titleSite;
HTML(file, page(file, title, krovy(), lang), pretty);

file = "altanky.html";
title = "Altánky, chatky" + titleSite;
HTML(file, page(file, title, altanky(), lang), pretty);

file = "doplnky.html";
title = "Doplnky, zrubové sedenia, záhradné lavičky" + titleSite;
HTML(file, page(file, title, doplnky(), lang), pretty);

file = "kontakt.html";
title = "Kontakt, Martin Rybár, Ján Albert" + titleSite;
HTML(file, page(file, title, kontakt(), lang), pretty);
