import { Hsmls, Hsml } from "prest-lib/dist/hsml";

export function index(): Hsmls {
    return [
        ["div.two-column", [
            ["div.pannel", [
                ["h1", [
                    ["a", { href: "drevodomy.html" }, "Drevodomy, Zruby"]
                ]],
                ["a", { href: "drevodomy.html" }, [
                    ["img", { alt: "drevodomy", src: "images/drevodomy.jpg" }] as Hsml
                ]],
                ["br"],
                "zruby, drevenice"
            ]]
        ]],
        ["div.two-column", [
            ["div.pannel", [
                ["h1", [
                    ["a", { href: "krovy.html" }, "Krovy"]
                ]],
                ["a", { href: "krovy.html" }, [
                    ["img", { alt: "krovy", src: "images/krovy.jpg" }] as Hsml
                ]],
                ["br"],
                "tesárske práce, krovy, strechy"
            ]]
        ]],
        ["div.two-column", [
            ["div.pannel", [
                ["h1", [
                    ["a", { href: "altanky.html" }, "Altánky"]
                ]],
                ["a", { href: "altanky.html" }, [
                    ["img", { alt: "altánky", src: "images/altanky.jpg" }] as Hsml
                ]],
                ["br"],
                "záhradné altánky, chatky, prístrešky"
            ]]
        ]],
        ["div.two-column", [
            ["div.pannel", [
                ["h1", [
                    ["a", { href: "doplnky.html" }, "Doplnky"]
                ]],
                ["a", { href: "doplnky.html" }, [
                    ["img", { alt: "doplnky", src: "images/doplnky.jpg" }] as Hsml
                ]],
                ["br"],
                "zrubové sedenia, záhradný nábytok"
            ]]
        ]],
        ["div.clear"],
        ["h2", "Realizujeme vaše sny..."],
        ["p", [
            "Celoročne obývateľné drevodomy, zruby, zrubové domy, chaty, drevenice, altánky,\n\t\t\t\tkrovy, zrubové sedenia, lavičky a iné doplnky z dreva\n\t\t\t\tpodľa požiadaviek zákazníka za",
            ["strong", "najlepšie ceny"],
            "."
        ]]
    ];
}
