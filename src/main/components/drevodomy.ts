import { Hsmls, Hsml } from "prest-lib/dist/hsml";

const images = [
        { src: "images/drevodomy/1.jpg", title: "Zrub" },
        { src: "images/drevodomy/2.jpg", title: "Zrub" },
        { src: "images/drevodomy/3.jpg", title: "Zrubový bungalov" },
        { src: "images/drevodomy/4.jpg", title: "Zrubový bungalov - skelet" },
        { src: "images/drevodomy/x.jpg", title: "Zrub - bungalov" },
        { src: "images/drevodomy/5.jpg", title: "Odstrešie zrubu" },
        { src: "images/drevodomy/6.jpg", title: "Steny zrubu" },
        { src: "images/drevodomy/7.jpg", title: "Vnútorná priečka zrubu z guľatiny" },
        { src: "images/drevodomy/8.jpg", title: "Zrubové schody" },
        { src: "images/drevodomy/11.jpg", title: "Skelet zrubu" },
        { src: "images/drevodomy/12.jpg", title: "Zrub Zbyňov" },
        { src: "images/drevodomy/13.jpg", title: "Zrub Bytča" },
        { src: "images/drevodomy/14.jpg", title: "Zrub Bytča" },
        { src: "images/drevodomy/15.jpg", title: "Zrub Lietavská Svinná" },
        { src: "images/drevodomy/16.jpg", title: "Skelet zrubovej garáže" },
        { src: "images/drevodomy/18.jpg", title: "Zrubová garáž" },
        { src: "images/drevodomy/10.jpg", title: "Elekroinštalácia v stene zrubu" },
        { src: "images/drevodomy/9.jpg", title: "Izolácia zrubu s pamäťovou páskou" },
        { src: "images/drevodomy/17.jpg", title: "Izolácia zrubovej steny" },
        { src: "images/drevodomy/19.jpg", title: "Termosnímka zrubového domu, exteriér pri teplote -13 °C" },
        { src: "images/drevodomy/20.jpg", title: "Termosnímka zrubového domu, interér pri teplote 7 °C" },
];

export function drevodomy(): Hsmls {
    return [
        ["h1",
            "Drevodomy, zruby a drevenice"
        ],
        ["p",
            "Štýlové tradičné drevodomy, kanadské zruby, zrubové domy, drevenice, rekreačné chalupy a chaty, jedinečné drevostavby aj pre komerčné využitie."
        ],
        ["div.gallery", [
            ["div#main_image"],
            ["ul.gallery_imgs",
                images.map<Hsml>((i, n) => (
                    ["li", { classes: [["active", !n]] }, [
                        ["img.xnoscale",
                            {
                                classes: [["active", !n]],
                                src: i.src,
                                alt: i.title,
                                title: i.title
                            }
                        ]
                    ]]
                ))
            ],
            ["p.nav"]
        ]],
        ["p", [
            "Drevodomy, zruby, zrubové domy, drevenice spájajú Vašu predstavu o zdravom bývaní a prírodné vlastnosti dreva do projektov rodinných domov alebo chát za výhodnú",
            ["a", { href: "#cena" }, "cenu"],
            ".",
            ["br"],
            "Každý zrubový dom je originálom pre každého zákazníka. Stavbu prispôsobíme Vašim potrebám a predstavám. Kvalitu výroby zaručuje naša viac ako desaťročná prax vo výrobe zrubov, dreveníc a chát."
        ]],
        ["h2", "Výhody zrubov a drevodomov"],
        ["ul", [
            ["li",
                "nízkoenergetické stavby, výborné tepelnoizolačné a akumulačné vlastnosti, energetické parametre zodpovedajú v štandarde nízkoenergetickej stavbe"
            ],
            ["li",
                "tradičná remeselná technológia výroby, prírodné materiály"
            ],
            ["li",
                "prirodzená regulácia vnútornej vlhkosti optimálnej pre zdravé bývanie"
            ],
            ["li",
                "environmentálne riešenie stavieb a pozitívny vplyv na ľudské zdravie"
            ],
            ["li",
                "životnosť porovnateľná so životnosťou murovanej stavby, 150 až 200 rokov"
            ],
            ["li",
                "jednoduchá údržba, spočíva v obnovovaní náteru každých 5 - 7 rokov"
            ],
            ["li",
                "stáročiami overené bývanie"
            ]
        ]],
        ["h2", "Stavba" ],
        ["p",
            "Každá stavba je originál a je prispôsobená potrebám a predstavám klienta. Na stavby používame výhradne kvalitný smrekový masív a rezivo. Tým sa dosiahne stabilita materiálu a obmedzia sa tvarové zmeny a vznik trhlín. Všetky drevené časti sú ošetrené ekologickými ochrannými povrchovými nátermi. Je možná kombinácia iných materiálov, častá je napríklad kombinácia kameňa a dreva."
        ],
        ["p",
            "Zrubová stavba sa vyrába tradičnou tesárskou technológiou sedlového spájania. Výroba trvá v závislosti od veľkosti stavby a náročnosti 1 – 3 mesiace. Môže realizovať súbežne so stavebným konaním, prípadne so výstavbou základov."
        ],
        ["p",
            "Doba montáže drevostavby priamo na stavenisku závisí od veľkosti stavby, od rozsahu dodávky – či sa jedná o hrubú stavbu, uzavretú hrubú stavbu alebo komplet stavbu s izoláciami; a od druhu použitých materiálov. Táto doba sa pohybuje v rozmedzí 1 – 8 týždňov."
        ],
        ["h3", "Strecha"],
        ["p",
            "Strešná konštrukcia je navrhnutá tak, aby zniesla záťaž aj tých najťažších strešných krytín ako je betónová či pálená škridla aj s bohatou snehovou pokrývkou. Je možné použiť všetky typy strešných krytín."
        ],
        ["h3", "Rozvody"],
        ["p",
            "Rozvody elektriny, vody a kúrenia sú uložené v konštrukčných priestoroch podlahy, stropu, stien a montovaných priečok takže nie sú viditeľné."
        ],
        ["h3", "Vykurovanie"],
        ["p",
            "V drevostavbe je možné použiť všetky bežné spôsoby vykurovania – teplovodné, krby či krbové kachle, tepelné čerpadlá. Najčastejšie používaná kombinácia je krb alebo krbové kachle a elektrické konvektory na temperovanie."
        ],
        ["h3", "Požiarna odolnosť"],
        ["p",
            "Drevo používané v stavebných konštrukciách ma triedu reakcie na oheň B - D podľa druhu a hustoty dreviny, vlhkosti, tvaru a opracovania povrchu. Požiarnu odolnosť je možné zvýšiť použitím ochranného náteru, ktorý znemožňuje proces horenia."
        ],
        ["div.info", [
            ["h2#cena", "Cenník"],
            ["p",
                "Cena sa vypočíta individuálne na základe veľkosti zrubového domu, typu použitých materiálov alebo na základe vypracovanej projektovej dokumentácie, ktorá zároveň slúži ako podklad pre vybavenie stavebného povolenia. V prípade záujmu Vám môžeme pripraviť vizualizáciu vašej budúcej stavby aj kompletnú projektovú dokumentáciu k stavebnému konaniu."
            ],
            ["p",
                "Orientačná cena zrubového domu:"
            ],
            ["ul", [
                ["li", [
                    ["strong", "skeletová konštrukcia"],
                    "v cene od:",
                    ["strong", ["298 €/m", ["sup", "2"] as Hsml]],
                    "+ DPH zastavanej plochy stavby (pôdorysná plocha stavby pod zrubom)"
                ]],
                ["li", [
                    ["strong", "hrubá stavba"],
                    "v cene od:",
                    ["strong", ["498 €/m", ["sup", "2"] as Hsml]],
                    "+ DPH úžitkovej plochy stavby"
                ]]
            ]],
            ["p",
                "Presnú cenu konkrétnej stavby je možné stanoviť na základe cenovej kalkulácie podľa projektovej dokumentácie a v závislosti od zvolených materiálov pre dokončovacie práce."
            ],
            ["h3", "Skelet"],
            ["p",
                "Stavba skeletu obsahuje:"
            ],
            ["ul", [
                ["li", "obvodové steny z guľatiny s priemerom od 300 - 400 mm"],
                ["li", "stropné hradovanie z guľatiny"],
                ["li", "vyrezanie a opracovanie otvorov pre okná a dvere"],
                ["li", "konštrukcia balkóna a terasy"],
                ["li", "krovová konštrukcia"],
                ["li", [
                    ["li", "vykostrenie štítov"],
                    ["li", "príprava pre elektrické rozvody"],
                    ["li", "izolácia v pozdĺžnych drážkach a rohových spojoch"],
                    ["li", "podkladový hranol"],
                    ["li", "spojovací materiál"],
                    ["li", "impregnácia bezfarebným náterom proti hnilobe, hmyzu a plesniam"],
                    ["li", "montáž"]
                ] as Hsmls]
            ]],
            ["p", [
                "Cena záleží podľa projektu, prosím",
                ["a", { href: "kontakt.html" }, "kontaktujte nás"], "."
            ]],
            ["h3", "Hrubá stavba"],
            ["p",
                "Hrubá stavba obsahuje:"
            ],
            ["ul", [
                ["li", "skelet"],
                ["li", "uzatvorenie stien štítov + izolácie"],
                ["li", "pokrytie strešnou krytinou"],
                ["li", "deliace priečky na prízemí a podkroví"],
                ["li", "základná povrchová úprava"],
                ["li", "okná, dvere"],
                ["li", "schody, zábradlie"],
                ["li", "vykostrenie podlahy"],
                ["li", "montáž"]
            ]],
            ["p", [
                "Cena záleží podľa projektu, prosím",
                ["a", { href: "kontakt.html" }, "kontaktujte nás"], "."
            ]],
            ["h3", "Stavba na kľúč:"],
            ["p",
                "Kompletné dokončenie stavby:"
            ],
            ["ul", [
                ["li", "skeletová konštrukcia"],
                ["li", "hrubá stavba"],
                ["li", "elektroinštalácie"],
                ["li", "rozvody vody, kanalizácie, sanita"],
                ["li", "vykurovanie"]
            ]],
            ["p", [
                "Cena záleží podľa projektu, prosím",
                ["a", { href: "kontakt.html" }, "kontaktujte nás"], "."
            ]]
        ]]
    ];
}
