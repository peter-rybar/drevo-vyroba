import { Hsmls, Hsml } from "prest-lib/dist/hsml";


const images = [
    { src: "images/krovy/1.jpg", title: "Krov bungalovu" },
    { src: "images/krovy/2.jpg", title: "Krov bungalovu" },
    { src: "images/krovy/3.jpg", title: "Konštrukcia krovu" },
    { src: "images/krovy/4.jpg", title: "Krov bungalovu" },
    { src: "images/krovy/5.jpg", title: "Krov s krytinonu tondach stodo" },
    { src: "images/krovy/6.jpg", title: "Sedlový krov" },
    { src: "images/krovy/7.jpg", title: "Krov s latovaním" },
    { src: "images/krovy/8.jpg", title: "Pokrytá strecha" },
    { src: "images/krovy/9.jpg", title: "Krov zrubu" }
];

export function krovy(): Hsmls {
    return [
        ["h1",
            "Krovy a strechy"
        ],
        ["p",
            "Krov je klasický a rokmi overený spôsob riešenia strechy. Súčasťou každého domu je kvalitný drevený krov."
        ],
        ["div.gallery", [
            ["div#main_image"],
            ["ul.gallery_imgs",
                images.map<Hsml>((i, n) => (
                    ["li", { classes: [["active", !n]] }, [
                        ["img",
                            { src: i.src, alt: i.title, title: i.title }
                        ]
                    ]]
                ))
            ],
            ["p.nav"]
        ]],
        ["p",
            "Drevená konštrukcia krovu je vhodná pre všetky typy strešnej krytiny. Tvar krovu je ovplyvnený tvarom strechy. Tvary a rozmery ovplyvňujú technické požiadavky, regionálne a architektonické hľadiská. Ponúkame Vám návrh krovu, jeho výrobu, a montáž krovu na stavbu."
        ],
        ["div.info", [
            ["h2",
                "Cenník"
            ],
            ["p",
                "Stavba kompletných krovov, podľa vyhotoveného projektu. Dodávka všetkých potrebných materiálov pre samotnú realizáciu."
            ],
            ["p", [
                "Cena krovu sa stanovuje od náročnosti a prevedenia konštrukcie.",
                ["br"],
                "Cena od 120 €/m", ["sup", "3"], ", prosím",
                ["a", { href: "kontakt.html" }, "kontaktujte nás"],
                "."
            ]]
        ]]
    ];
}
