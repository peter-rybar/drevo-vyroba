import { Hsmls, Hsml } from "prest-lib/dist/hsml";

const images = [
    { src: "images/doplnky/1.jpg", title: "Zrubový stôl" },
    { src: "images/doplnky/2.jpg", title: "Zrubové sedenie" },
    { src: "images/doplnky/3.jpg", title: "Zrubové sedenie" },
    { src: "images/doplnky/4.jpg", title: "Zrubové sedenie" },
    { src: "images/doplnky/5.jpg", title: "Zrubový kvetináč" },
    { src: "images/doplnky/6.jpg", title: "Zrubové kvetináče" }
];

export function doplnky(): Hsmls {
    return [
        ["h1",
            "Doplnky a záhradný nábytok"
        ],
        ["p",
            "Záhradné doplnky a nábytok alebo záhradné sedenia sú vyrábané z rôznych materiálov,\n\t\t\t\tktoré predurčujú umiestnenie nábytku na terasu, do altánku, prístrešku\n\t\t\t\talebo pod holé nebo.\n\t\t\t\tZáhradný nábytok by mal byť dostatočne masívny a pevný aby odolal vplyvom počasia."
        ],
        ["div.gallery", [
            ["div#main_image"],
            ["ul.gallery_imgs",
                images.map<Hsml>((i, n) => (
                    ["li", { classes: [["active", !n]] }, [
                        ["img.xnoscale",
                            {
                                classes: [["active", !n]],
                                src: i.src,
                                alt: i.title,
                                title: i.title
                            }
                        ]
                    ]]
                ))
            ],
            ["p.nav"]
        ]],
        ["div.info", [
            ["h2", "Cenník"],
            ["p", [
                "Cena sa odvíja od množstva použitého materiálu,\n\t\t\t\t\tprosím",
                ["a", { href: "kontakt.html" }, "kontaktujte nás"], "."
            ]]
        ]]
    ];
}
