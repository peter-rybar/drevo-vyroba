import { Hsmls } from "prest-lib/dist/hsml";

export function kontakt(): Hsmls {
    return [
        ["h1",
            "Kontakt"
        ],
        ["div.two-column", [
            ["div.pannel", [
                ["div#hcard-Martin-Rybar-Centaur.vcard", [
                    ["h2.fn", "Martin Rybár"],
                    ["div", [
                        ["em", "email:"],
                        ["a.email",
                            { href: "mailto:mr.rybar@gmail.com" },
                            "mr.rybar@gmail.com"
                        ]
                    ]],
                    ["div", [
                        ["em", "telefón:"],
                        ["span.tel", "+421 904 416 460"]
                    ] as Hsmls],
                    // ["div",
                    //     "web:",
                    //     ["a.url",
                    //         {
                    //             href: "http://drevo-vyroba.appspot.com"
                    //         },
                    //         "http://drevo-vyroba.appspot.com"
                    //     ]
                    // ],
                    ["br"],
                    ["div.adr", [
                        ["div.street-address", "Hurbanova 557/10"],
                        ["span.locality", "Rajec"],
                        ",",
                        ["span.postal-code", "015 01"],
                        ["div.country-name", "Slovensko"]
                    ] as Hsmls]
                ]]
            ]]
        ]],
        ["div.two-column", [
            ["div.pannel", [
                ["div#hcard-Jan-Albert-Centaur.vcard", [
                    ["h2.fn", "Ján Albert"],
                    ["div", [
                        ["em", "email:"],
                        ["a.email", { href: "mailto:janci816@gmail.com" },
                            "janci816@gmail.com"
                        ]
                    ] as Hsmls],
                    ["div", [
                        ["em", "telefón:"],
                        ["span.tel", "+421 903 566 204"]
                    ] as Hsmls],
                    // ["div",
                    //     "web:",
                    //     ["a.url",
                    //         {
                    //             href: "http://drevo-vyroba.appspot.com"
                    //         },
                    //         "http://drevo-vyroba.appspot.com"
                    //     ]
                    // ],
                    ["br"],
                    ["div.adr", [
                        ["div.street-address", "Pri Rajčanke 141"],
                        ["span.locality", "Zbyňov"],
                        ",",
                        ["span.postal-code", "013 19"],
                        ["div.country-name", "Slovensko"]
                    ] as Hsmls]
                ]]
            ]]
        ]],
        // ["div.panel",
        //     "{% if form.sent %}\n\t\t\t\tMail sent\n{% else %}",
        //     ["form",
        //         {
        //             "action": "",
        //             "method": "post"
        //         },
        //         ["div",
        //             ["input",
        //                 {
        //                     "type": "text",
        //                     "name": "name",
        //                     "placeholder": "Meno",
        //                     "value": "{{ form.name }}",
        //                     "size": "30",
        //                     "maxlength": "70"
        //                 }
        //             ],
        //             "Meno"
        //         ],
        //         ["div.error",
        //             "{{ form.name_error }}"
        //         ],
        //         ["div",
        //             ["input",
        //                 {
        //                     "type": "email",
        //                     "name": "email",
        //                     "placeholder": "E-mail",
        //                     "value": "{{ form.email }}",
        //                     "size": "30",
        //                     "maxlength": "70"
        //                 }
        //             ],
        //             "E-mail"
        //         ],
        //         ["div.error",
        //             "{{ form.email_error }}"
        //         ],
        //         ["div",
        //             ["textarea",
        //                 {
        //                     "name": "text",
        //                     "rows": "3",
        //                     "cols": "60"
        //                 },
        //                 "{{ form.text }}"
        //             ]
        //         ],
        //         ["div.error",
        //             "{{ form.text_error }}"
        //         ],
        //         ["div",
        //             ["input",
        //                 {
        //                     "type": "submit",
        //                     "value": "Odoslať"
        //                 }
        //             ]
        //         ]
        //     ],
        //     "{% endif %}"
        // ],
    ];
}
