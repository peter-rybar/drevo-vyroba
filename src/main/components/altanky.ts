import { Hsmls, Hsml } from "prest-lib/dist/hsml";

const images = [
    { src: "images/altanky/1.jpg", title: "Altánot" },
    { src: "images/altanky/2.jpg", title: "Konštrukcia altánku" },
    { src: "images/altanky/3.jpg", title: "Altánok" },
    { src: "images/altanky/4.jpg", title: "Podhľad altánku" },
    { src: "images/altanky/5.jpg", title: "Altánok s guľatými stĺpmi" },
    { src: "images/altanky/6.jpg", title: "Prístrešok na drevo" },
    { src: "images/altanky/7.jpg", title: "Podhľad prístrešku na drevo" }
];

export function altanky(): Hsmls {
    return [
        ["h1",
            "Altánky a prístrešky"
        ],
        ["p",
            "Záhradné altánky sú ideálne do každej záhrady pre príjemné vonkajšie posedenie.\n\t\t\t\tVytvárajú intimitu pri posedeniach a oslavách aj keď vás prekvapí nepriaznivé počasie."
        ],
        ["div.gallery", [
            ["div#main_image"],
            ["ul.gallery_imgs",
                images.map<Hsml>((i, n) => (
                    ["li", { classes: [["active", !n]] }, [
                        ["img.xnoscale",
                            {
                                classes: [["active", !n]],
                                src: i.src,
                                alt: i.title,
                                title: i.title
                            }
                        ] as Hsml
                    ]]
                ))
            ],
            ["p.nav"]
        ]],
        ["h2",
            "Altánky"
        ],
        ["p",
            "Altánky sú prirodzene pôsobiace a veľmi praktické stavby.\n\t\t\t\tArchitektúru altánku sa snažíme vyriešiť individuálne tak, aby kompozične zapadla\n\t\t\t\tdo exteriéru a korešpondovala s jestvujúcou zástavbou a flórou,\n\t\t\t\ts cieľom dosiahnuť prirodzene pôsobiaci objekt."
        ],
        ["h2",
            "Prístrešky pre autá"
        ],
        ["p",
            "V prípade, kde parametre pozemku a celkovej architektúry rodinného domu\n\t\t\t\tnedovoľujú postaviť užívateľovi garáž, je najvhodnejšie riešenie prístrešok pre auto.\n\t\t\t\tJeho tvar sa dodatočne prispôsobí architektúre jestvujúcej stavby\n\t\t\t\ta tak celkový vzhľad pôsobí kompaktne."
        ],
        ["div.info", [
            ["h2",
                "Cenník"
            ],
            ["p", [
                "Ako konštrukčný materiál na výrobu altánkov v štandarde používame\n\t\t\t\t\thobľované smrekové rezivo o hrúbkach od 100x100 mm do 160x160 mm.\n\t\t\t\t\tCena sa stanovuje od veľkosti, náročnosti jednotlivej stavby a použitých materiálov.\n\t\t\t\t\tOrientačná cena od: 64 €/m",
                ["sup", "2"]
            ]],
            ["p", [
                "Cena záleží podľa projektu, prosím",
                ["a", { href: "kontakt.html" }, "kontaktujte nás"], "."
            ]]
        ]]
    ];
}
