import { Hsmls, Hsml } from "prest-lib/dist/hsml";

export function page(file: string,
                     title: string,
                     content: Hsmls,
                     lang = "en"): Hsmls {
    return [
        "<!DOCTYPE html>",
        ["html", { lang }, [
            ["head", [
                ["meta", { "charset": "utf-8" }],
                ["meta", { name: "google-site-verification", content: "k4-u0C-JVP441nNzVjhoguhaW0WpU1pBfQIvd93UgkM" }],
                ["meta", { "http-equiv": "X-UA-Compatible", content: "IE=edge,chrome=1" }],
                ["meta", { name: "viewport", content: "width=800" }],
                ["meta", { name: "robots", content: "index,follow" }],
                ["meta", { name: "author", content: "Peter Rybar, pr.rybar@gmail.com" }],
                ["title", title],
                ["meta", { name: "description", content: title }],
                ["meta", { name: "keywords", content: title }],
                ["link", { rel: "stylesheet", title: "Screen", media: "screen,print", type: "text/css", href: "css/screen.css" }],
                ["link", { rel: "stylesheet", title: "Screen", media: "screen,print", type: "text/css", href: "css/screen_content.css" }],
                ["link", { rel: "stylesheet", title: "Screen", media: "screen,print", type: "text/css", href: "css/galleria.css" }],
                ["link", { rel: "stylesheet", title: "Screen", media: "screen,print", type: "text/css", href: "css/gallery.css" }],
                ["script", { src: "javascript/jquery.min.js" }],
                ["script", { src: "javascript/jquery.galleria.js" }],
                ["script", { src: "javascript/gallery.js" }],
                ["script", { src: "https://www.googletagmanager.com/gtag/js?id=UA-126399328-1", async: "" }],
                ["script", [
                        "window.dataLayer = window.dataLayer || [];",
                        "function gtag(){dataLayer.push(arguments);}",
                        "gtag('js', new Date());",
                        "gtag('config', 'UA-126399328-1');"
                    ].join("")
                ]
            ]],
            ["body", [
                ["div#outer", [
                    ["div#upbg"],
                    ["div#inner", [
                        ["div#header", [
                            ["h1", [
                                ["a", { href: "./" }, [
                                    "Drevo", ["span", "Výroba"] as Hsml
                                ]]
                            ]],
                            ["h2",
                                "Martin Rybár, Ján Albert"
                            ]
                        ]],
                        ["div#splash"],
                        ["div#menu", [
                            ["ul", [
                                ["li", [
                                    ["a.first",
                                        {
                                            classes: file === "drevodomy.html" ? ["active"] : [],
                                            href: "drevodomy.html",
                                            title: "Alt-d",
                                            accesskey: "d",
                                            rel: "tag"
                                        },
                                        "Drevodomy"
                                    ]
                                ]],
                                ["li", [
                                    ["a",
                                        {
                                            classes: file === "krovy.html" ? ["active"] : [],
                                            href: "krovy.html",
                                            title: "alt-k",
                                            accesskey: "k",
                                            rel: "tag"
                                        },
                                        "Krovy"
                                    ]
                                ]],
                                ["li", [
                                    ["a",
                                        {
                                            classes: file === "altanky.html" ? ["active"] : [],
                                            href: "altanky.html",
                                            title: "alt-a",
                                            accesskey: "a",
                                            rel: "tag"
                                        },
                                        "Altánky"
                                    ]
                                ]],
                                ["li", [
                                    ["a",
                                        {
                                            classes: file === "doplnky.html" ? ["active"] : [],
                                            href: "doplnky.html",
                                            title: "alt-z",
                                            accesskey: "z",
                                            rel: "tag"
                                        },
                                        "Doplnky"
                                    ]
                                ]],
                                ["li", [
                                    ["a",
                                        {
                                            classes: file === "kontakt.html" ? ["active"] : [],
                                            href: "kontakt.html",
                                            title: "Alt-c",
                                            accesskey: "c",
                                            rel: "tag"
                                        },
                                        "Kontakt"
                                    ]
                                ]]
                            ]],
                            ["div#lang", [
                                ["strong", { "title": "Slovenčina" }, "sk"]
                            ]]
                        ]],
                        ["div#content",
                            content
                        ],
                        ["div.clear"],
                        ["div#footer", [
                            ["em", [
                                ["a", { href: "https://gitlab.com/peter-rybar/drevo-vyroba" },
                                    "GitLab"
                                ]
                            ]]
                        ]]
                    ]]
                ]]
            ]]
        ]]
    ];
}
